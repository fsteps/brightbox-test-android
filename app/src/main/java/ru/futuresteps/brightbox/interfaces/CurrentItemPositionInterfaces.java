package ru.futuresteps.brightbox.interfaces;

/**
 * Created by Николай on 19.12.2016.
 */
public interface CurrentItemPositionInterfaces {

    public int getCurrentItemPosition();
    public void setCurrentItemPosition(int currentItemPosition);

}
