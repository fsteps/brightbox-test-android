package ru.futuresteps.brightbox.controllers;

import ru.futuresteps.brightbox.entities.NewsItem;

import java.util.LinkedList;

/**
 * Created by Николай on 20.12.2016.
 */
public class NewsController {
    private static LinkedList<NewsItem> mNewsItemList = null;

    public static LinkedList<NewsItem> getNewsItemList()
    {
        if (mNewsItemList == null)
            mNewsItemList = new LinkedList<>();
        return mNewsItemList;
    }

    public static NewsItem getNewsItemById(String newsId)
    {
        LinkedList<NewsItem> newsItemList = getNewsItemList();

        for (NewsItem newsItem : newsItemList)
        {
            if (newsItem.getNewsId().equals(newsId))
                return newsItem;
        }

        return null;
    }
}
