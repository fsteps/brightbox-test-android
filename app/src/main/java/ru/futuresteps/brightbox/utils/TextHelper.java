package ru.futuresteps.brightbox.utils;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;

/**
 * Created by Николай on 20.12.2016.
 */
public class TextHelper {
    public static Spanned getSpannedTextFromHtmlString(String html)
    {
        if (Build.VERSION.SDK_INT >= 24) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }
}
