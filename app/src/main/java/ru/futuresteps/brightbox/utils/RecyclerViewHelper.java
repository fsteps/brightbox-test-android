package ru.futuresteps.brightbox.utils;

import android.os.Handler;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Николай on 20.12.2016.
 */
public class RecyclerViewHelper {
    public static void postAndNotifyAdapter(final Handler handler, final RecyclerView recyclerView, final RecyclerView.Adapter adapter) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (!recyclerView.isComputingLayout()) {
                    adapter.notifyDataSetChanged();
                } else {
                    postAndNotifyAdapter(handler, recyclerView, adapter);
                }
            }
        });
    }

    public static void postAndNotifyAdapter(final Handler handler, final RecyclerView recyclerView, final RecyclerView.Adapter adapter, final int position) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (!recyclerView.isComputingLayout()) {
                    //adapter.notifyDataSetChanged();
                    adapter.notifyItemChanged(position);
                } else {
                    postAndNotifyAdapter(handler, recyclerView, adapter, position);
                }
            }
        });
    }
}
