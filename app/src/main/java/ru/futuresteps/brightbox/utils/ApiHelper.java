package ru.futuresteps.brightbox.utils;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Target;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.futuresteps.brightbox.BrightboxApplication;
import ru.futuresteps.brightbox.interfaces.CurrentItemPositionInterfaces;
import ru.futuresteps.brightbox.loaders.BrightboxApi;

/**
 * Created by Николай on 19.12.2016.
 */
public class ApiHelper {
    private static String BaseUrl = "http://dvc.brightbox.ru:8080/api/v4/services/";
    private static BrightboxApi brightboxApi = null;

    public static BrightboxApi getBrightboxApi ()
    {
        if (brightboxApi == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BaseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            brightboxApi = retrofit.create(BrightboxApi.class);
        }
        return brightboxApi;
    }

    public static void loadImage(String url, final Target target, int width, int height)
    {
        if (url != null && !url.isEmpty()) {
            Picasso picasso = Picasso.with(BrightboxApplication.getAppContext());
            picasso.setLoggingEnabled(true);
            RequestCreator requestCreator = picasso.load(url);
            if (width > 0 && height > 0)
                requestCreator.resize(width, height);
            requestCreator.into(target);
        }
    }
}
