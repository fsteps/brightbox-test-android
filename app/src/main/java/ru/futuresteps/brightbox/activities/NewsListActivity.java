package ru.futuresteps.brightbox.activities;

import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.futuresteps.brightbox.R;
import ru.futuresteps.brightbox.adapters.NewsListAdapter;
import ru.futuresteps.brightbox.controllers.NewsController;
import ru.futuresteps.brightbox.entities.Feed;
import ru.futuresteps.brightbox.entities.NewsItem;
import ru.futuresteps.brightbox.utils.ApiHelper;
import ru.futuresteps.brightbox.utils.RecyclerViewHelper;

import java.util.LinkedList;

public class NewsListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, Callback<Feed>
{
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_view_news)
    RecyclerView recyclerViewNews;
    @BindView(R.id.progress_bar_for_news)
    ProgressBar progressBarNews;

    private RecyclerView.Adapter mNewsListAdapter;
    private LinearLayoutManager mLayoutManagerNews;

    final Integer ApiTakeParam = 10;
    Integer ApiSkipParam = 0;
    boolean isAllLoaded = false;

    private boolean newsListLoading = true;

    private int previousTotal = 0;

    private int visibleThreshold = 5;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    int pastVisiblesItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_list);
        ButterKnife.bind(this);

        swipeRefreshLayout.setOnRefreshListener(this);

        //recyclerViewNews.setHasFixedSize(true);

        mLayoutManagerNews = new LinearLayoutManager(this);
        recyclerViewNews.setLayoutManager(mLayoutManagerNews);

        mNewsListAdapter = new NewsListAdapter(NewsController.getNewsItemList(), recyclerViewNews);
        recyclerViewNews.setAdapter(mNewsListAdapter);

        recyclerViewNews.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if(dy > 0)
                {
                    visibleItemCount = recyclerViewNews.getChildCount();
                    totalItemCount = mLayoutManagerNews.getItemCount();
                    pastVisiblesItems = mLayoutManagerNews.findFirstVisibleItemPosition();

                    if (!newsListLoading)
                    {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            progressBarNews.setVisibility(View.VISIBLE);
                            loadFeed();
                        }
                    }
                }
            }
        });

        refresh();
    }

    /* SwipeRefreshLayout.OnRefreshListener */

    @Override
    public void onRefresh() {
        refresh();
    }

    /* * */

    /* Callback<Feed> */

    @Override
    public void onResponse(Call<Feed> call, Response<Feed> response) {
        Feed feed = response.body();
        LinkedList<NewsItem> news = feed.getNewsItemList();

        if (ApiSkipParam == 0)
        {
            NewsController.getNewsItemList().clear();
        }
        NewsController.getNewsItemList().addAll(news);
        ApiSkipParam += news.size();
        if (news.size() < ApiTakeParam)
            isAllLoaded = true;

        NewsListAdapter newsListAdapter = (NewsListAdapter)recyclerViewNews.getAdapter();
        //newsListAdapter.setList(news);

        Handler handler = new Handler();
        RecyclerViewHelper.postAndNotifyAdapter(handler, recyclerViewNews, newsListAdapter);
        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
        else progressBarNews.setVisibility(View.GONE);
        newsListLoading = false;
    }

    @Override
    public void onFailure(Call<Feed> call, Throwable t) {
        Toast.makeText(this, "Internet Error " + t.getMessage(), Toast.LENGTH_SHORT).show();

        NewsController.getNewsItemList().clear();
        NewsListAdapter newsListAdapter = (NewsListAdapter)recyclerViewNews.getAdapter();
        newsListAdapter.notifyDataSetChanged();

        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
        else progressBarNews.setVisibility(View.GONE);
        newsListLoading = false;
    }

    /* * */

    protected void refresh()
    {
        swipeRefreshLayout.setRefreshing(true);
        ApiSkipParam = 0;
        isAllLoaded = false;
        loadFeed();
    }

    protected void loadFeed()
    {
        newsListLoading = true;
        Call<Feed> call = ApiHelper.getBrightboxApi().loadFeed("93f8bf8f-9bac-407f-b93c-ea232d109543", ApiTakeParam.toString(), ApiSkipParam.toString());
        call.enqueue(this);
    }

}
