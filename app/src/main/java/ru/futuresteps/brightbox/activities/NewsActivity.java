package ru.futuresteps.brightbox.activities;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.futuresteps.brightbox.R;
import ru.futuresteps.brightbox.adapters.NewsListAdapter;
import ru.futuresteps.brightbox.controllers.NewsController;
import ru.futuresteps.brightbox.entities.Feed;
import ru.futuresteps.brightbox.entities.NewsDetail;
import ru.futuresteps.brightbox.entities.NewsItem;
import ru.futuresteps.brightbox.loaders.BrightboxApi;
import ru.futuresteps.brightbox.utils.ApiHelper;
import ru.futuresteps.brightbox.utils.RecyclerViewHelper;
import ru.futuresteps.brightbox.utils.TextHelper;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Николай on 19.12.2016.
 */
public class NewsActivity extends AppCompatActivity implements Callback<NewsDetail> {

    public final static String CURRENT_NEWS_POSITION_PARAMETER = "CurrentNewsPositionParam";

    @BindView(R.id.lbl_news_title)
    TextView textViewNewsTitle;
    @BindView(R.id.lbl_news_text)
    TextView textViewNewsText;
    @BindView(R.id.progress_bar_for_thumbnail)
    ProgressBar progressBarThumbnail;
    @BindView(R.id.image_placeholder)
    ImageView imageViewNewsPlaceholder;

    private int mCurrentPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();

        mCurrentPosition = extras.getInt(CURRENT_NEWS_POSITION_PARAMETER);

        loadNews();
    }

    /* Callback<NewsDetail> */

    @Override
    public void onResponse(Call<NewsDetail> call, Response<NewsDetail> response) {
        NewsDetail newsDetail = response.body();
        if (newsDetail != null) {
            NewsItem newsItem = NewsController.getNewsItemById(newsDetail.getNewsId());
            newsItem.setNewsDetail(newsDetail);

            ShowNews(newsDetail);
            /*if (swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);
            else progressBarNews.setVisibility(View.GONE);
            newsListLoading = false;*/
        }
    }

    @Override
    public void onFailure(Call<NewsDetail> call, Throwable t) {
        Toast.makeText(this, "Error " + t.getMessage(), Toast.LENGTH_SHORT).show();

        /*NewsController.getNewsItemList().clear();
        NewsListAdapter newsListAdapter = (NewsListAdapter)recyclerViewNews.getAdapter();
        newsListAdapter.notifyDataSetChanged();

        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
        else progressBarNews.setVisibility(View.GONE);
        newsListLoading = false;*/
    }

    /* * */

    protected void ShowNews(NewsDetail newsDetail)
    {
        progressBarThumbnail.setVisibility(View.GONE);
        textViewNewsTitle.setText(newsDetail.getTitle());
        textViewNewsText.setText(TextHelper.getSpannedTextFromHtmlString(newsDetail.getText()));

    }

    protected void loadNews()
    {
        NewsItem newsItem = NewsController.getNewsItemList().get(mCurrentPosition);
        NewsDetail newsDetail = newsItem.getNewsDetails();
        if (newsDetail == null) {
            progressBarThumbnail.setVisibility(View.VISIBLE);
            Call<NewsDetail> call = ApiHelper.getBrightboxApi().loadNews(newsItem.getNewsId());
            call.enqueue(this);
        } else
        {
            ShowNews(newsDetail);
        }
    }
}
