package ru.futuresteps.brightbox.entities;

/**
 * Created by Николай on 19.12.2016.
 */
public class Thumbnail {

    public Thumbnail(String url, Double acpectRatio) {
        Url = url;
        AcpectRatio = acpectRatio;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public Double getAcpectRatio() {
        return AcpectRatio;
    }

    public void setAcpectRatio(Double acpectRatio) {
        AcpectRatio = acpectRatio;
    }

    private String Url;
    private Double AcpectRatio;
}
