package ru.futuresteps.brightbox.entities;

import android.graphics.Bitmap;

/**
 * Created by Николай on 18.12.2016.
 */
public class NewsItem {

    public NewsItem(String newsId, String title, String subtitle, Thumbnail thumbnail) {
        NewsId = newsId;
        Title = title;
        Subtitle = subtitle;
        Thumbnail = thumbnail;
    }


    public String getNewsId() {
        return NewsId;
    }

    public void setNewsId(String newsId) {
        NewsId = newsId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getSubtitle() {
        return Subtitle;
    }

    public void setSubtitle(String subtitle) {
        Subtitle = subtitle;
    }

    public int getImagesCount() {
        return ImagesCount;
    }

    public void setImagesCount(int imagesCount) {
        ImagesCount = imagesCount;
    }

    public ru.futuresteps.brightbox.entities.Thumbnail getThumbnail() {
        return Thumbnail;
    }

    public void setThumbnail(ru.futuresteps.brightbox.entities.Thumbnail thumbnail) {
        Thumbnail = thumbnail;
    }


    public Bitmap getThumbnailBitmap() {
        return ThumbnailBitmap;
    }

    public void setThumbnailBitmap(Bitmap thumbnailBitmap) {
        ThumbnailBitmap = thumbnailBitmap;
    }

    public NewsDetail getNewsDetails() {
        return NewsDetail;
    }

    public void setNewsDetail(NewsDetail newsDetail) {
        NewsDetail = newsDetail;
    }

    private String NewsId;
    private String Title;
    private String Subtitle;
    private int ImagesCount;
    private Thumbnail Thumbnail;
    private Bitmap ThumbnailBitmap;
    private NewsDetail NewsDetail;
}
