package ru.futuresteps.brightbox.entities;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Николай on 18.12.2016.
 */
public class Feed {

    public Feed (int count, List<FeedData> data)
    {
        this.Count = count;
        this.Data = data;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        this.Count = count;
    }



    public List<FeedData> getData() {
        return Data;
    }

    public void setData(List<FeedData> data) {
        this.Data = data;
    }

    public LinkedList<NewsItem> getNewsItemList() {
        LinkedList<NewsItem> newsList = new LinkedList<>();
        for (FeedData item : Data) {
            newsList.add(item.getNewsItem());
        }
        return newsList;
    }

    private int Count;
    private List<FeedData> Data;
}
