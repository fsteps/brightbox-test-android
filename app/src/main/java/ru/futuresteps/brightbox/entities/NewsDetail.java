package ru.futuresteps.brightbox.entities;

import java.util.List;

/**
 * Created by Николай on 16.12.2016.
 */
public class NewsDetail {

    public NewsDetail(String newsId, String title, String text, List<GalleryImage> gallery) {
        NewsId = newsId;
        Title = title;
        Text = text;
        Gallery = gallery;
    }

    public String getNewsId() {
        return NewsId;
    }

    public void setNewsId(String newsId) {
        NewsId = newsId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public List<GalleryImage> getGallery() {
        return Gallery;
    }

    public void setGallery(List<GalleryImage> gallery) {
        Gallery = gallery;
    }

    private String NewsId;
    private String Title;
    private String Text;
    private List<GalleryImage> Gallery;
}
