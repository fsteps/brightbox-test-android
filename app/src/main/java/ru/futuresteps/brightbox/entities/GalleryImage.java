package ru.futuresteps.brightbox.entities;

/**
 * Created by Николай on 20.12.2016.
 */
public class GalleryImage {
    public GalleryImage(String url) {
        Url = url;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    private String Url;
}
