package ru.futuresteps.brightbox.entities;

/**
 * Created by Николай on 18.12.2016.
 */
public class FeedData {//Сойдет для тестового задания, чтобы не писать свой Serializer
    public FeedData(NewsItem newsItem) {
        this.NewsItem = newsItem;
    }

    public NewsItem getNewsItem() {
        return NewsItem;
    }

    public void setNewsItem(NewsItem newsItem) {
        this.NewsItem = newsItem;
    }

    private NewsItem NewsItem;
}
