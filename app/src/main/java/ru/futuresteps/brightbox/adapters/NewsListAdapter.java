package ru.futuresteps.brightbox.adapters;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import ru.futuresteps.brightbox.R;
import ru.futuresteps.brightbox.activities.NewsActivity;
import ru.futuresteps.brightbox.entities.NewsItem;
import ru.futuresteps.brightbox.entities.Thumbnail;
import ru.futuresteps.brightbox.interfaces.CurrentItemPositionInterfaces;
import ru.futuresteps.brightbox.utils.ApiHelper;
import ru.futuresteps.brightbox.utils.RecyclerViewHelper;
import ru.futuresteps.brightbox.utils.TextHelper;

import java.util.LinkedList;

/**
 * Created by Николай on 16.12.2016.
 */
public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.ViewHolder>{

    private LinkedList<NewsItem> mNewsItemList;
    private RecyclerView mRecyclerView;

    public NewsListAdapter(LinkedList<NewsItem> newsList, RecyclerView recyclerView) {
        mNewsItemList = newsList;
        mRecyclerView = recyclerView;
    }

    @Override
    public NewsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_news_list, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setCurrentItemPosition(position);
        holder.setNewsItemList(mNewsItemList);
        onBindViewHolder(holder, position, this);
    }

    protected void onBindViewHolder(final ViewHolder holder, final int position, final NewsListAdapter newsListAdapter)
    {
        final NewsItem news = mNewsItemList.get(position);

        holder.textViewNewsTitle.setText(news.getTitle());
        holder.textViewNewsSubtitle.setVisibility(View.GONE);
        holder.progressBarThumbnail.setVisibility(View.INVISIBLE);

        Log.i("onBindViewHolder", "Позиция " + String.valueOf(position) + " getImagesCount=" + String.valueOf(news.getImagesCount()));

        if (news.getImagesCount() > 0) {
            if (news.getThumbnailBitmap() == null) {

                holder.imageViewNewsPlaceholder.setImageDrawable(null);

                holder.imageViewNewsPlaceholder.getViewTreeObserver()
                        .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            // Wait until layout to call Picasso
                            @Override
                            public void onGlobalLayout() {
                                // Ensure we call this only once
                                holder.imageViewNewsPlaceholder.getViewTreeObserver()
                                        .removeOnGlobalLayoutListener(this);

                                final Target target = new Target() {
                                    @Override
                                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                        Log.i("onBindViewHolder ", "onBitmapLoaded Позиция " + String.valueOf(position) + " from=" + from.toString());
                                        if (position == holder.getCurrentItemPosition()) {

                                            holder.imageViewNewsPlaceholder.setImageBitmap(bitmap);
                                            holder.progressBarThumbnail.setVisibility(View.GONE);
                                            try {
                                                Log.i("onBindViewHolder ", "in onBitmapLoaded Позиция " + String.valueOf(position));
                                                Handler handler = new Handler();
                                                RecyclerViewHelper.postAndNotifyAdapter(handler, mRecyclerView, newsListAdapter, position);
                                            } catch (Exception ex) {
                                                Log.i("onBindViewHolder ", "in onBitmapLoaded Позиция " + String.valueOf(position) + " Exception=" + ex.toString() );
                                            }

                                        }
                                        news.setThumbnailBitmap(bitmap);
                                    }

                                    @Override
                                    public void onBitmapFailed(Drawable errorDrawable) {
                                        Log.i("onBindViewHolder ", "onBitmapFailed Позиция " + String.valueOf(position));
                                        if (position == holder.getCurrentItemPosition()) {
                                            holder.textViewNewsSubtitle.setVisibility(View.VISIBLE);
                                            holder.textViewNewsSubtitle.setText(TextHelper.getSpannedTextFromHtmlString(news.getSubtitle()));
                                            holder.progressBarThumbnail.setVisibility(View.GONE);
                                            try {
                                                Log.i("onBindViewHolder ", "in onBitmapFailed Позиция " + String.valueOf(position));

                                                Handler handler = new Handler();
                                                RecyclerViewHelper.postAndNotifyAdapter(handler, mRecyclerView, newsListAdapter, position);
                                            } catch (Exception ex) {
                                                Log.i("onBindViewHolder ", "in onBitmapFailed Позиция " + String.valueOf(position) + " Exception=" + ex.toString() );
                                            }
                                        }
                                        news.setImagesCount(0);
                                    }

                                    @Override
                                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                                        if (position == holder.getCurrentItemPosition()) {
                                            holder.progressBarThumbnail.setVisibility(View.VISIBLE);
                                            holder.imageViewNewsPlaceholder.setImageDrawable(placeHolderDrawable);
                                        }
                                    }
                                };

                                holder.imageViewNewsPlaceholder.setTag(target);

                                Thumbnail thumbnail = news.getThumbnail();
                                int width = holder.imageViewNewsPlaceholder.getWidth();
                                int height = thumbnail.getAcpectRatio() > 0 ? (int)Math.round(width/thumbnail.getAcpectRatio()) : 0;

                                Log.i("onBindViewHolder", "Позиция " + String.valueOf(position)
                                        + " getThumbnail=" + thumbnail.getUrl()
                                        + " width=" + String.valueOf(width)
                                        + " height=" + String.valueOf(height));

                                ApiHelper.loadImage(news.getThumbnail().getUrl(), target, width, height);
                            }
                        });
            } else {
                holder.imageViewNewsPlaceholder.setImageBitmap(news.getThumbnailBitmap());
            }
        } else
        {
            holder.imageViewNewsPlaceholder.setImageDrawable(null);
            holder.progressBarThumbnail.setVisibility(View.GONE);
            holder.textViewNewsSubtitle.setVisibility(View.VISIBLE);
            holder.textViewNewsSubtitle.setText(TextHelper.getSpannedTextFromHtmlString(news.getSubtitle()));
        }
    }

    /* * */

    /*@Override
    public void onDestroy() {  // could be in onPause or onStop
        Picasso.with(this).cancelRequest(target);
        super.onDestroy();
    }*/

    /* * */

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mNewsItemList.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder implements CurrentItemPositionInterfaces, View.OnClickListener  {
        @BindView(R.id.lbl_news_title)
        TextView textViewNewsTitle;
        @BindView(R.id.lbl_news_subtitle)
        TextView textViewNewsSubtitle;
        @BindView(R.id.progress_bar_for_thumbnail)
        ProgressBar progressBarThumbnail;
        @BindView(R.id.image_placeholder)
        ImageView imageViewNewsPlaceholder;

        private int currentItemPosition;
        private LinkedList<NewsItem> mNewsItemList;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            ButterKnife.bind(this, view);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(view.getContext(), NewsActivity.class);
            intent.putExtra(NewsActivity.CURRENT_NEWS_POSITION_PARAMETER, currentItemPosition);
            view.getContext().startActivity(intent);
        }

        public void setNewsItemList(LinkedList<NewsItem> newsItemList)
        {
            this.mNewsItemList = newsItemList;
        }

        /* CurrentItemPositionInterfaces */

        @Override
        public int getCurrentItemPosition()
        {
            return currentItemPosition;
        }

        @Override
        public void setCurrentItemPosition(int currentItemPosition)
        {
            this.currentItemPosition = currentItemPosition;
        }

        /* * */
    }
}
