package ru.futuresteps.brightbox.loaders;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.futuresteps.brightbox.entities.Feed;
import ru.futuresteps.brightbox.entities.NewsDetail;
import ru.futuresteps.brightbox.entities.NewsItem;

import java.util.List;

/**
 * Created by Николай on 18.12.2016.
 */
public interface BrightboxApi {
    @Headers({
            "apikey: M2Y3YTkxNjliNjAxODg4YzU4NjQwMjA3ZTk2M2RjMTdjN2ZlNmQ4Nw==",
            "Authorization: Basic NGVmODA5ZjgtYTU5Ny00YmUxLWIwOGQtOWViZmEwNjBjODY5Ozo="
    })
    @GET("getfeed")
    Call<Feed> loadFeed(@Query("providers") String providers, @Query("take") String take, @Query("skip") String skip);

    @Headers({
            "apikey: M2Y3YTkxNjliNjAxODg4YzU4NjQwMjA3ZTk2M2RjMTdjN2ZlNmQ4Nw==",
            "Authorization: Basic NGVmODA5ZjgtYTU5Ny00YmUxLWIwOGQtOWViZmEwNjBjODY5Ozo="
    })
    @GET("getnews/{newsId}")
    Call<NewsDetail> loadNews(@Path("newsId") String newsId);
}
