package ru.futuresteps.brightbox;

import android.app.Application;
import android.content.Context;

/**
 * Created by Николай on 19.12.2016.
 */
public class BrightboxApplication extends Application {
    private static Context context;

    public static Context getAppContext()
    {
        return BrightboxApplication.context;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        BrightboxApplication.context = getApplicationContext();
    }
}
